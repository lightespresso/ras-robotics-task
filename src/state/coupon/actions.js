import { createTypes, async, actionOf } from 'utils/redux'
import { postData } from 'services/echo-server'

export const types = createTypes('restaurants/', async('FETCH'))

export function postCouponData ({ name, email }) {
  return async function (dispatch, getState) {
    dispatch(actionOf(types.FETCH, null))
    try {
      await postData({ name, email })
      dispatch(actionOf(types.FETCH_SUCCESS, null))
    } catch (error) {
      console.log(error)
      dispatch(actionOf(types.FETCH_FAIL, null))
    }
  }
}
