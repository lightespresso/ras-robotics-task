import { routerReducer } from 'react-router-redux'
import { reducer as formReducer } from 'redux-form'
import { combineReducers } from 'redux'

import hotel from './hotel/reducers'
import restaurants from './restaurants/reducers'
import weather from './weather/reducers'

export default combineReducers({
  router: routerReducer,
  form: formReducer,
  hotel,
  restaurants,
  weather
})
