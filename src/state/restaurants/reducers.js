import { createReducer } from 'utils/redux'
import { types } from './actions'
import { always, prop } from 'ramda'

const restaurants = createReducer(null, {
  [types.FETCH]: always(null),
  [types.FETCH_SUCCESS]: prop('results'),
  [types.FETCH_FAIL]: always(null)
})

export default restaurants
