import { createTypes, async, actionOf } from 'utils/redux'
import { getHotel } from 'state/hotel/selectors'
import { getNearestPlaces } from 'services/google-maps'

export const types = createTypes('restaurants/', async('FETCH'))

export function fetchRestaurants () {
  return async function (dispatch, getState) {
    const state = getState()
    const { lat, lng } = getHotel(state)
    dispatch(actionOf(types.FETCH, null))
    try {
      const restaurants = await getNearestPlaces({
        lat,
        lng,
        type: 'restaurant'
      })
      dispatch(actionOf(types.FETCH_SUCCESS, restaurants))
    } catch (error) {
      console.log(error)
      dispatch(actionOf(types.FETCH_FAIL, error))
    }
  }
}
