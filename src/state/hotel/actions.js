import { createTypes, actionOf } from 'utils/redux'

export const types = createTypes('hotel/', 'SELECT')

export const selectHotel = actionOf(types.SELECT)
