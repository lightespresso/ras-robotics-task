export const HOTELS = {
  tlv: {
    id: 'tlv',
    city: 'Tel Aviv',
    lat: 32.109333,
    lng: 34.855499
  },
  ny: {
    id: 'ny',
    city: 'New York',
    lat: 40.73061,
    lng: -73.935242
  }
}
