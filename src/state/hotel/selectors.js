import { createSelector } from 'utils/redux'

import { HOTELS } from './utils'

export const getHotelId = state => state.hotel
export const getHotel = createSelector(getHotelId, id => HOTELS[id])
