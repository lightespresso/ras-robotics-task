import { always } from 'ramda'
import { createReducer } from 'utils/redux'
import { types } from './actions'

import { HOTELS } from './utils'

const hotel = createReducer(HOTELS.tlv.id, {
  [types.SELECT]: always
})

export default hotel
