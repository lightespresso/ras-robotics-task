import { createReducer } from 'utils/redux'
import { types } from './actions'
import { always } from 'ramda'

const weather = createReducer(null, {
  [types.FETCH]: always(null),
  [types.FETCH_SUCCESS]: always,
  [types.FETCH_FAIL]: always(null)
})

export default weather
