import { createTypes, async, actionOf } from 'utils/redux'
import { getHotel } from 'state/hotel/selectors'
import { getWeather } from 'services/apixu'

export const types = createTypes('weather/', async('FETCH'))

export function fetchWeather () {
  return async function (dispatch, getState) {
    const state = getState()
    const { city } = getHotel(state)
    dispatch(actionOf(types.FETCH, null))
    try {
      const weather = await getWeather({ city })
      dispatch(actionOf(types.FETCH_SUCCESS, weather))
    } catch (error) {
      console.log(error)
      dispatch(actionOf(types.FETCH_FAIL, error))
    }
  }
}
