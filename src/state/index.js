import { compose, createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { createBrowserHistory } from 'history'
import { routerMiddleware as createRouterMiddleware } from 'react-router-redux'
import { DEV } from 'config'

import rootReducer from './reducers'

const composeEnhansers =
  (DEV && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose

export const history = createBrowserHistory()
const routerMiddleware = createRouterMiddleware(history)

export function configureStore () {
  const store = createStore(
    rootReducer,
    composeEnhansers(applyMiddleware(thunkMiddleware, routerMiddleware))
  )

  return store
}
