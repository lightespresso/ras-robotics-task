import React from 'react'
import ReactDOM from 'react-dom'

import { configureStore, history } from './state'
import App from './App'

import 'roboto-fontface/css/roboto/roboto-fontface.css'

const store = configureStore()

ReactDOM.render(
  <App store={store} history={history} />,
  document.getElementById('root')
)
