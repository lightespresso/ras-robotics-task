import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Table, TableBody, TableHead, TableRow, TableCell } from 'material-ui'
import { connect } from 'utils/redux'
import { fetchWeather } from 'state/weather/actions'
import { getWeather } from 'state/weather/selectors'

import CardView from 'components/card-view'
import Loader from 'components/loader'

class Weather extends Component {
  static propTypes = {
    fetchWeather: PropTypes.func.isRequired,
    weather: PropTypes.array
  }
  componentDidMount () {
    this.props.fetchWeather()
  }
  renderWeather = () => {
    const { weather } = this.props
    return (
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Date</TableCell>
            <TableCell>Temp, C</TableCell>
            <TableCell>Condition</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>{weather.map(this.renderWeatherDay)}</TableBody>
      </Table>
    )
  }
  renderWeatherDay = ({ temp, date, condition, id }) => {
    return (
      <TableRow key={id}>
        <TableCell>{date}</TableCell>
        <TableCell>{temp}, C</TableCell>
        <TableCell>{condition}</TableCell>
      </TableRow>
    )
  }
  render () {
    const { weather } = this.props
    const isLoading = weather === null
    return (
      <CardView title="Weather forecast" showBack>
        {isLoading ? <Loader /> : this.renderWeather()}
      </CardView>
    )
  }
}

export default connect(
  {
    weather: getWeather
  },
  { fetchWeather }
)(Weather)
