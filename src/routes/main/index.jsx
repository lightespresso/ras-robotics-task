import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'utils/redux'
import { getHotelId } from 'state/hotel/selectors'
import { selectHotel } from 'state/hotel/actions'
import { HOTELS } from 'state/hotel/utils'

import { Menu, MenuItem } from 'components/menu'
import Select from 'components/select'
import CardView from 'components/card-view'

const HOTEL_OPTIONS = Object.keys(HOTELS).map(id => {
  const { city } = HOTELS[id]
  return { value: id, label: city }
})

Main.propTypes = {
  hotelId: PropTypes.string.isRequired,
  selectHotel: PropTypes.func.isRequired
}

function Main ({ hotelId, selectHotel }) {
  return (
    <CardView title="Hello! How can I help you?">
      <Select
        value={hotelId}
        onChange={e => selectHotel(e.target.value)}
        options={HOTEL_OPTIONS}
      />
      <Menu>
        <MenuItem to="/restaurants" label="Show restaurants" />
        <MenuItem to="/weather" label="Weather" />
        <MenuItem to="/coupon" label="Get free SPA coupon" />
      </Menu>
    </CardView>
  )
}

export default connect({ hotelId: getHotelId }, { selectHotel })(Main)
