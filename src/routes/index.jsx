import React from 'react'
import PropTypes from 'prop-types'
import { Switch, Route } from 'react-router'
import { ConnectedRouter } from 'react-router-redux'

import Main from './main'
import Restaurants from './restaurants'
import Weather from './weather'
import Coupon from './coupon'

Routes.propTypes = {
  history: PropTypes.object.isRequired
}

function Routes ({ history }) {
  return (
    <ConnectedRouter history={history}>
      <Switch>
        <Route path="/restaurants" component={Restaurants} />
        <Route path="/weather" component={Weather} />
        <Route path="/coupon" component={Coupon} />
        <Route path="/" component={Main} />
      </Switch>
    </ConnectedRouter>
  )
}

export default Routes
