import React, { Component } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { Field, reduxForm } from 'redux-form'
import { Button } from 'material-ui'

import { compose } from 'ramda'
import { connect } from 'utils/redux'
import { push } from 'react-router-redux'
import { isRequired, isEmail } from 'utils/form-validation'
import { postCouponData } from 'state/coupon/actions'

import Input from 'components/form-fields/input'
import CardView from 'components/card-view'

class Coupon extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    postCouponData: PropTypes.func.isRequired
  }
  async onSubmit (data) {
    const { postCouponData, push } = this.props
    await postCouponData(data)
    push('/')
  }
  render () {
    return (
      <CardView title="Fill the form to get free coupon" showBack>
        <Form onSubmit={this.props.handleSubmit(this.onSubmit)}>
          <Field
            name="name"
            title="Name"
            component={Input}
            validate={isRequired}
          />
          <Field
            name="email"
            title="Email"
            component={Input}
            validate={[isRequired]}
          />
          <Button
            type="submit"
            onClick={this.props.handleSubmit(this.onSubmit)}
          >
            Submit
          </Button>
        </Form>
      </CardView>
    )
  }
}

export default compose(
  connect(null, { push, postCouponData }),
  reduxForm({
    form: 'couponForm'
  })
)(Coupon)

const Form = styled.form`
  width: 40%;
  display: flex;
  flex-direction: column;
  margin: 2rem 0;
`
