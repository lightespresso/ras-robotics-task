import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'utils/redux'
import { fetchRestaurants } from 'state/restaurants/actions'
import { getRestaurants } from 'state/restaurants/selectors'
import { getHotel } from 'state/hotel/selectors'

import CardView from 'components/card-view'
import Map from 'components/map'
import Loader from 'components/loader'

class Restaurants extends Component {
  static propTypes = {
    fetchRestaurants: PropTypes.func.isRequired,
    hotel: PropTypes.object.isRequired,
    restaurants: PropTypes.array
  }
  componentDidMount () {
    this.props.fetchRestaurants()
  }
  getMarkers () {
    const { restaurants } = this.props
    return restaurants.map(({ geometry, name, id }) => {
      const { location } = geometry
      return { label: name, id, ...location }
    })
  }
  render () {
    const { restaurants, hotel } = this.props
    const isLoading = restaurants === null
    return (
      <CardView title="Nearest restaurants" showBack>
        {isLoading ? (
          <Loader />
        ) : (
          <Map showPosition position={hotel} markers={this.getMarkers()} />
        )}
      </CardView>
    )
  }
}

export default connect(
  {
    restaurants: getRestaurants,
    hotel: getHotel
  },
  { fetchRestaurants }
)(Restaurants)
