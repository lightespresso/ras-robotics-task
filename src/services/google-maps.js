import axios from 'axios'
import { prop } from 'ramda'
import { GM_API_KEY } from 'config'

export const getNearestPlaces = ({ lat, lng, type, radius = 3000 }) =>
  axios
    .get(
      `https://crossorigin.me/https://maps.googleapis.com/maps/api/place/nearbysearch/json`,
      {
        params: {
          key: GM_API_KEY,
          location: `${lat},${lng}`,
          type,
          radius
        }
      }
    )
    .then(prop('data'))
