import axios from 'axios'
import { prop } from 'ramda'

export const postData = data =>
  axios.post('http://scooterlabs.com/echo', { data }).then(prop('data'))
