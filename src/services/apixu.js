import axios from 'axios'
import { prop } from 'ramda'
import { APIXU_API_KEY } from 'config'

export const getWeather = ({ city }) =>
  axios
    .get(`http://api.apixu.com/v1/forecast.json`, {
      params: {
        key: APIXU_API_KEY,
        q: city,
        days: 5
      }
    })
    .then(prop('data'))
    .then(parseWeatherData)

const parseWeatherData = ({ forecast }) =>
  forecast.forecastday.map(({ date_epoch, date, day }) => ({
    temp: day.avgtemp_c,
    id: date_epoch,
    date,
    condition: day.condition.text
  }))
