import React from 'react'
import PropTypes from 'prop-types'
import { TextField } from 'material-ui'

InputField.propTypes = {
  input: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired
}

function InputField ({ input, title, meta, ...props }) {
  const error = meta.touched && meta.error
  return (
    <TextField
      error={Boolean(error)}
      label={error || title}
      {...input}
      {...props}
    />
  )
}

export default InputField
