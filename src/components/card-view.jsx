import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { Card, Button } from 'material-ui'
import { Backspace } from 'material-ui-icons'

CardView.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.array]),
  showBack: PropTypes.bool
}

function CardView ({ title, showBack, children, ...props }) {
  return (
    <Outer>
      <Inner {...props}>
        <h2>
          {title}
          {showBack && (
            <Back component={Link} to="/">
              Go back
            </Back>
          )}
        </h2>
        {children}
      </Inner>
    </Outer>
  )
}

export default CardView

const Outer = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
`

const Inner = styled(Card)`
  padding: 0 2rem;
  min-width: 728px;
  width: 40vw;
`

const Back = styled(Button)`
  margin-left: 1rem;
`
