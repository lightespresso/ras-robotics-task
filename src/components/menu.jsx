import React from 'react'
import PropTypes from 'prop-types'
import { List, ListItem, ListItemText } from 'material-ui'
import { Link } from 'react-router-dom'
import inject from 'hocs/inject'

MenuItem.propTypes = {
  label: PropTypes.string.isRequired,
  to: PropTypes.string
}

export function MenuItem ({ label, to, ...props }) {
  return (
    <ListItem button component={Link} to={to} {...props}>
      <ListItemText primary={label} />
    </ListItem>
  )
}

export const Menu = inject({ component: 'div' }, List)
