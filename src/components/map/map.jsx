import React from 'react'
import PropTypes from 'prop-types'
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker
} from 'react-google-maps'
import { compose } from 'ramda'
import inject from 'hocs/inject'
import { GM_API_KEY } from 'config'

Map.propTypes = {
  position: PropTypes.shape({
    lat: PropTypes.number,
    lng: PropTypes.number
  }).isRequired,
  markers: PropTypes.arrayOf(
    PropTypes.shape({
      lat: PropTypes.number,
      lng: PropTypes.number,
      label: PropTypes.string
    })
  ),
  showPosition: PropTypes.bool
}

const renderMarker = marker => {
  const { lat, lng, id } = marker
  return <Marker position={{ lat, lng }} key={id} />
}

function Map ({ position, markers = [] }) {
  return (
    <GoogleMap defaultZoom={12} defaultCenter={position}>
      {markers.map(renderMarker)}
    </GoogleMap>
  )
}

export default compose(
  inject({
    googleMapURL: `https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=${
      GM_API_KEY
    }`,
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px`, marginBottom: '2rem' }} />,
    mapElement: <div style={{ height: `100%` }} />
  }),
  withScriptjs,
  withGoogleMap
)(Map)
