import React from 'react'
import PropTypes from 'prop-types'
import { Select as MaterialSelect, MenuItem } from 'material-ui'

Select.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      value: PropTypes.any
    })
  )
}

const renderOption = option => (
  <MenuItem key={option.value} value={option.value}>
    {option.label}
  </MenuItem>
)

function Select ({ options, ...props }) {
  return <MaterialSelect {...props}>{options.map(renderOption)}</MaterialSelect>
}

export default Select
