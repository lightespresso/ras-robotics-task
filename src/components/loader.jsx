import styled from 'styled-components'
import ReactLoading from 'react-loading'

const Loader = styled(ReactLoading).attrs({
  type: 'spinningBubbles',
  color: `#666`
})`
  margin: 2rem auto;
`

export default Loader
