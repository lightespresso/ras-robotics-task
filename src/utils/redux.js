import { connect as ReactReduxConnect } from 'react-redux'
import { always, uncurryN, map, curry } from 'ramda'
import { createStructuredSelector } from 'reselect'

/**
 * @type {(type: string, payload: any) => {type: string, payload: any}}
 */
export const actionOf = curry((type, payload) => {
  if (!type) {
    throw new TypeError('Action type is undefined')
  }
  return { type, payload }
})

/**
 * Creates a reducer
 * @param {Any} initialState
 * @param {Object.<String, Function>} handlers { [type]: (payload => state) => newState }
 */
export const createReducer = (initialState, handlers) => {
  const uncurriedHandlers = map(uncurryN(2), handlers)
  return function (state = initialState, action) {
    const handler = uncurriedHandlers[action.type] || always(state)
    return handler(action.payload, state)
  }
}

export const connect = (selectors, actionCreators, mergeProps) =>
  ReactReduxConnect(
    selectors && createStructuredSelector(selectors),
    actionCreators,
    mergeProps
  )

export { createSelector } from 'reselect'
export { createTypes, async } from 'redux-action-types'
