import isemail from 'isemail'

export const isRequired = val => (val ? undefined : 'required')

export const isEmail = val =>
  val && isemail.validate(val) ? undefined : 'wrong email format'
