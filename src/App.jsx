import React from 'react'
import PropTypes from 'prop-types'
import { Provider } from 'react-redux'

import Routes from './routes'

App.propTypes = {
  history: PropTypes.object.isRequired,
  store: PropTypes.object.isRequired
}

function App ({ history, store }) {
  return (
    <Provider store={store}>
      <Routes history={history} />
    </Provider>
  )
}

export default App
