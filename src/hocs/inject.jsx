import React from 'react'
import { curry } from 'ramda'

function inject (injectProps, Component) {
  const WrappedComponent = props => <Component {...injectProps} {...props} />
  return WrappedComponent
}

/**
 * @type {inject}
 */
export default curry(inject)
